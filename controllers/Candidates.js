'use strict';

var utils = require('../utils/writer.js');
var Candidates = require('../service/CandidatesService');

module.exports.addCandidate = function addCandidate (req, res, next) {
  var candidate = req.swagger.params['Candidate'].value;
  Candidates.addCandidate(candidate)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.readCandidate = function readCandidate (req, res, next) {
  var skills = req.swagger.params['skills'].value;
  Candidates.readCandidate(skills)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};