'use strict';

var Model = require('./Model');

/**
 * create
 * a desc
 *
 * candidate Candidate a desc
 * no response value expected for this operation
 **/
exports.addCandidate = function(candidate) {
  return new Promise(function(resolve, reject) {

    Model.datamodel.push(candidate);

    //console.log(Model.datamodel);
    resolve();
  });
}


/**
 * read
 * a desc
 *
 * skills List skills array
 * returns Candidate
 **/
exports.readCandidate = function(skills) {
  return new Promise(function(resolve, reject) {

    let counts = [];

    for(var i=0; i<Model.datamodel.length; i++){

      let currentCount = 0;
      for(var j=0; j<skills.length; j++){

        if(Model.datamodel[i].skills.includes(skills[j])){
          ++currentCount;
        }
      }
      counts.push(currentCount);
    }

    let index = 0;
    let value = 0;
    for(var k=0; k<counts.length; k++){

      if(counts[k]>value){
        value = counts[k];
        index = k;
      }
    }    

    if (value > 0) {
      resolve(Model.datamodel[index]);
    } else {
      resolve();
    }
  });
}

